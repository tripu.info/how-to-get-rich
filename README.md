**<img src="https://gitlab.com/tripu.info/how-to-get-rich/-/raw/master/assets/diamond.png" alt="Project logo" width="24" /> How to get rich (without getting lucky)**

_A presentation of Naval Ravikant's [homonymous tweetstorm](https://mobile.twitter.com/naval/status/1002103360646823936)_

👉 [Check it out](https://tripu.info.gitlab.io/how-to-get-rich/) 👈

Made with [Node.js/npm](https://nodejs.org/en/), [Pug](https://pugjs.org/), [Sass](https://sass-lang.com/) and [TypeScript](https://www.typescriptlang.org/).

Icons made by [Freepik](https://www.flaticon.com/authors/freepik) from [www.flaticon.com](https://www.flaticon.com/)

---

Integrate https://nav.al/rich

https://styles.redditmedia.com/t5_1094sp/styles/backgroundImage_iztrypkw3mu21.png  
(background of https://www.reddit.com/r/NavalRavikant/ )
