{
  'use strict';

  let position:number = 0;
  let stack:Array<HTMLElement> = [];
  let sections:Array<HTMLElement> = [];
  let backLink:HTMLElement;
  let prevLink:HTMLElement;
  let nextLink:HTMLElement;

  const toggleNavItem = (item:HTMLElement, visible:boolean):void => {
    if(visible) {
      item.style.display = 'block';
      item.style.visibility = 'visible';
    } else {
      item.style.display = 'none';
      item.style.visibility = 'hidden';
    }
  };

  const updateNavItems = ():void => {
    if(stack.length) {
      toggleNavItem(prevLink, false);
      toggleNavItem(nextLink, false);
      toggleNavItem(backLink, true);
    } else {
      toggleNavItem(backLink, false);
      toggleNavItem(prevLink, position > 0);
      toggleNavItem(nextLink, position < sections.length -1);
    }
  };

  const goBack = (e:Event):void => {
    e.preventDefault();
    const from:HTMLElement = stack.pop();
    from.classList.remove('current');
    let to:HTMLElement;
    if(stack.length)
      to = stack.slice(-1)[0];
    else
      to = sections[position];
    to.classList.add('current');
    updateNavItems();
  };

  const goPrev = (e:Event):void => {
    e.preventDefault();
    const from:HTMLElement = sections[position--];
    from.classList.remove('current');
    const to:HTMLElement = sections[position];
    to.classList.add('current');
    updateNavItems();
  };

  const goNext = (e:Event):void => {
    e.preventDefault();
    const from:HTMLElement = sections[position++];
    from.classList.remove('current');
    const to:HTMLElement = sections[position];
    to.classList.add('current');
    updateNavItems();
  };
    
  const digDeeper = (e:string|Event):void => {
    let to:HTMLElement;
    let from:HTMLElement;
    if ('object' === typeof e) {
      e.preventDefault();
      to = document.getElementById((e.target as HTMLAnchorElement).attributes.getNamedItem('href').value);
    } else
      to = document.getElementById(e);
    if (stack.length)
      from = stack.slice(-1)[0];
    else
      from = sections[position];
    from.classList.remove('current');
    to.classList.add('current');
    stack.push(to);
    updateNavItems();
  };

  const prepareForNavigation = (a:HTMLAnchorElement):void => {
    if(!a.href)
      a.href = a.innerText;
    a.addEventListener('click', digDeeper);
  };

  const init = () => {
    const links:NodeList = document.querySelectorAll<HTMLAnchorElement>('section a');
    const rawSections:NodeList = document.querySelectorAll<HTMLElement>('.main');
    backLink = document.getElementById('back');
    prevLink = document.getElementById('prev');
    nextLink = document.getElementById('next');
    backLink.querySelector('a').addEventListener('click', goBack);
    prevLink.querySelector('a').addEventListener('click', goPrev);
    nextLink.querySelector('a').addEventListener('click', goNext);
    rawSections.forEach(s => sections.push(s as HTMLElement));
    links.forEach(prepareForNavigation);
  };

  addEventListener('load', init, {once: true});
}
